package br.com.empresa.api.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.empresa.api.entity.Tier;

public interface TierRepo extends CrudRepository<Tier, Long> {

}
