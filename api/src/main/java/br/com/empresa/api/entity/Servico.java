package br.com.empresa.api.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Servico {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Long id;

	@Column
	private String nomeServico;

	@Column
	private Double valorBase;

	@Column
	private Integer diasDesconto;

	@Column
	private Double porcentoDesconto;

	@OneToMany(mappedBy = "servico", cascade = CascadeType.ALL)
	private List<Contrato> contratos;

	@Column
	private Boolean ativo;

	public Servico() {
	}

	public Servico(String nomeServico, Double valorBase, Integer diasDesconto, Double porcentoDesconto,
			List<Contrato> contratos, Boolean ativo) {
		this.nomeServico = nomeServico;
		this.valorBase = valorBase;
		this.diasDesconto = diasDesconto;
		this.porcentoDesconto = porcentoDesconto;
		this.contratos = contratos;
		this.ativo = ativo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNomeServico() {
		return nomeServico;
	}

	public void setNomeServico(String nomeServico) {
		this.nomeServico = nomeServico;
	}

	public Double getValorBase() {
		return valorBase;
	}

	public void setValorBase(Double valorBase) {
		this.valorBase = valorBase;
	}

	public Integer getDiasDesconto() {
		return diasDesconto;
	}

	public void setDiasDesconto(Integer diasDesconto) {
		this.diasDesconto = diasDesconto;
	}

	public Double getPorcentoDesconto() {
		return porcentoDesconto;
	}

	public void setPorcentoDesconto(Double porcentoDesconto) {
		this.porcentoDesconto = porcentoDesconto;
	}

	public List<Contrato> getContratos() {
		return contratos;
	}

	public void setContratos(List<Contrato> contratos) {
		this.contratos = contratos;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ativo == null) ? 0 : ativo.hashCode());
		result = prime * result + ((contratos == null) ? 0 : contratos.hashCode());
		result = prime * result + ((diasDesconto == null) ? 0 : diasDesconto.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nomeServico == null) ? 0 : nomeServico.hashCode());
		result = prime * result + ((porcentoDesconto == null) ? 0 : porcentoDesconto.hashCode());
		result = prime * result + ((valorBase == null) ? 0 : valorBase.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Servico other = (Servico) obj;
		if (ativo == null) {
			if (other.ativo != null)
				return false;
		} else if (!ativo.equals(other.ativo))
			return false;
		if (contratos == null) {
			if (other.contratos != null)
				return false;
		} else if (!contratos.equals(other.contratos))
			return false;
		if (diasDesconto == null) {
			if (other.diasDesconto != null)
				return false;
		} else if (!diasDesconto.equals(other.diasDesconto))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nomeServico == null) {
			if (other.nomeServico != null)
				return false;
		} else if (!nomeServico.equals(other.nomeServico))
			return false;
		if (porcentoDesconto == null) {
			if (other.porcentoDesconto != null)
				return false;
		} else if (!porcentoDesconto.equals(other.porcentoDesconto))
			return false;
		if (valorBase == null) {
			if (other.valorBase != null)
				return false;
		} else if (!valorBase.equals(other.valorBase))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Servico [id=" + id + ", nomeServico=" + nomeServico + ", valorBase=" + valorBase + ", diasDesconto="
				+ diasDesconto + ", porcentoDesconto=" + porcentoDesconto + ", contratos=" + contratos + ", ativo="
				+ ativo + "]";
	}

}
