package br.com.empresa.api.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.empresa.api.entity.Contrato;

@RepositoryRestResource(path = "contratos", collectionResourceRel = "contratos")
public interface ContratoRepo extends CrudRepository<Contrato, Long> {

}
