package br.com.empresa.api.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.empresa.api.entity.Servico;

@RepositoryRestResource(path = "servicos", collectionResourceRel = "servicos")
public interface ServicoRepo extends CrudRepository<Servico, Long> {

}
