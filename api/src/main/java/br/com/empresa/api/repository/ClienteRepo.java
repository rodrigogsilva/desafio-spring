package br.com.empresa.api.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.empresa.api.entity.Cliente;

public interface ClienteRepo extends CrudRepository<Cliente, Long> {

}
