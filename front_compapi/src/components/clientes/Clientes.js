import React, { useContext } from 'react';
import ApiContext from '../../context/api/ApiContext';
import Cliente from './Cliente';

const Clientes = () => {
    const apiContext = useContext(ApiContext);
    const { clientes } = apiContext;

    return (
        <div style={userStyle}>
            {clientes.map(cliente => (
                <Cliente key={cliente.id} user={cliente} />
            ))}
        </div>
    );
};

const userStyle = {
    display: 'grid',
    gridTemplateColumns: 'repeat(3, 1fr)',
    grdGap: '1rem',
};

export default Clientes;
