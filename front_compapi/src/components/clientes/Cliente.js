import React, { Fragment, useEffect, useContext } from 'react';
import ApiContext from '../../context/api/ApiContext';

const Cliente = () => {
    const apiContext = useContext(ApiContext);

    const { cliente } = apiContext;

    const { nomeCliente, emailCliente, telefone, ativo } = cliente;

    return (
        <Fragment>
            <div>
                <h1>{nomeCliente}</h1>
                <p>Email: {emailCliente}</p>
                <p>Telefone: {telefone}</p>
                <p>Ativo: {ativo ? 'Sim' : 'Não'}</p>
            </div>
        </Fragment>
    );
};

export default Cliente;
