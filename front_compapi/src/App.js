import React from 'react';
import Cliente from './components/clientes/Cliente';

const App = () => {
    return (
        <div>
            <Cliente />
        </div>
    );
};

export default App;
