import React, { useReducer } from "react";
import axios from "axios";
import GithubContext from "./GithubContext";
import GithubReducer from "./GithubReducer";
import PropTypes from "proto-type";
import { SEARCH_USERS, SET_LOADING } from "../types";

let urlApi;

if (process.env.NODE_ENV !== "production") {
  urlApi = process.env.REACT_APP_API_URL;
}
const GithubState = props => {
  GithubState.propTypes = {
    children: PropTypes.object.isRequired
  };

  const initialState = {
    users: [],
    user: {},
    repos: [],
    loading: false
  };

  const [state, dispatch] = useReducer(GithubReducer, initialState);

  const searchUsers = async () => {
    setLoading();
    const res = await axios.get(urlApi + "/clientes");

    dispatch({
      type: SEARCH_USERS,
      payload: res.data.items
    });
  };

  const setLoading = () => dispatch({ type: SET_LOADING });

  return (
    <GithubContext.Provider
      value={{
        users: state.users,
        loading: state.loading,
        searchUsers
      }}
    >
      {props.children}
    </GithubContext.Provider>
  );
};

export default GithubState;
