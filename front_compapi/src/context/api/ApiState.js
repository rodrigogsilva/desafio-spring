import React, { useReducer } from 'react';
import axios from 'axios';
import ApiContext from './ApiContext';
import ApiReducer from './ApiReducer';
import { SEARCH_USERS } from '../types';

const ApiState = props => {
    let apiURL;

    if (process.env.NODE_ENV !== 'production') {
        apiURL = process.env.REACT_APP_API_URL;
    } else {
    }

    const initialState = {
        clientes: [],
        cliente: {},
        loading: false,
    };

    const [state, dispatch] = useReducer(ApiReducer, initialState);

    // Search Github users
    const searchUsers = async () => {
        const res = await axios.get(`${apiURL}`);

        dispatch({
            type: SEARCH_USERS,
            payload: res.data._embedded.clientes.map(cliente => cliente),
        });
    };

    return (
        <ApiContext.Provider
            value={{
                clientes: state.clientes,
                cliente: state.cliente,
                searchUsers,
            }}>
            {props.children}
        </ApiContext.Provider>
    );
};

export default ApiState;
