import { SEARCH_USERS, GET_USER } from '../types';

export default (state, action) => {
    switch (action.type) {
        case SEARCH_USERS:
            return { ...state, users: action.payload, loading: false };
        case GET_USER:
            return { ...state, user: action.payload, loading: false };

        default:
            break;
    }
};
