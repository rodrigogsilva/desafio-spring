import React, { useContext } from "react";
import EmpresaContext from "../context/EmpresaApi/EmpresaContext";

const Home = () => {
  const empresaContext = useContext(EmpresaContext);
  const { searchUsers, users } = empresaContext;
  searchUsers();
  console.log(users);

  return (
    <div>
      {users.map(user => (
        <h1 key="1">teste,user</h1>
      ))}
    </div>
  );
};

export default Home;
